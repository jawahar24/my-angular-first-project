import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrouprepoComponent } from './grouprepo.component';

describe('GrouprepoComponent', () => {
  let component: GrouprepoComponent;
  let fixture: ComponentFixture<GrouprepoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrouprepoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrouprepoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
