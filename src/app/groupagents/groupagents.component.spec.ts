import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupagentsComponent } from './groupagents.component';

describe('GroupagentsComponent', () => {
  let component: GroupagentsComponent;
  let fixture: ComponentFixture<GroupagentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupagentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupagentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
